"""Model to train Faster R-CNN"""
import os
from collections import defaultdict

import torch
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone

from ...with_device import WithDeviceMixin
from ..utils import get_optimizer, get_scheduler, get_weight_init_func, \
    attempt_use_apex
from ..losses import IoULoss, multibox_iou
from .faster_rcnn import FasterRCNN

USE_APEX, AMP = attempt_use_apex()

class FasterRCNNModel(WithDeviceMixin):
    """Model to train Faster R-CNN"""
    def __init__(self, options):
        super().__init__()
        self.options = options
        self.loss_dict = defaultdict(int)
        self.evaluation_loss = IoULoss(
            iou_func=multibox_iou, reduction='none').to(self.device)

        self.net = FasterRCNN(
            resnet_fpn_backbone('resnet101', False),
            num_classes=options.num_classes).to(self.device)
        self.net.apply(get_weight_init_func(self.options))
        self.optimizer = get_optimizer(
            self.options,
            [param
             for param in self.net.parameters() if param.requires_grad],
            net_role="detection")
        self.scheduler = get_scheduler(self.optimizer, self.options)

        if USE_APEX and self.options.use_mixed_precision:
            self.net, self.optimizer = AMP.initialize(self.net, self.optimizer,
                                                      opt_level="O1")

        self.optimizers = {"detection": self.optimizer}
        # create folder for storage
        if not os.path.isdir(self.options.save_path):
            os.makedirs(self.options.save_path)

    def pretrain(self, *args, **kwargs):
        """Needed for interface purposes"""

    def _loss_backward(self, loss):
        if USE_APEX and self.options.use_mixed_precision:
            with AMP.scale_loss(loss, self.optimizer) as scaled_loss:
                scaled_loss.backward()
        else:
            loss.backward()
        self.optimizer.step()

    def update_learning_rates(self):
        """Update learning rates at the end of an epoch"""
        self.scheduler.step()

    def __str__(self):
        string = f"{self.net}\n"
        string += "Total number of parameters for model: "
        string += f"{self.net.get_number_params()}"
        return string

    def train_epoch(self, data, image_paths):
        """Train one epoch of the model"""
        image = data["image"]
        bboxes = data["boxes"]
        boxes = []
        for index in range(len(image_paths)):
            boxes.append({
                "boxes": bboxes["boxes"][index],
                "labels": bboxes["labels"][index]})

        loss_dict = self._get_loss_dict(*self.net(image, boxes), boxes)
        for key, value in loss_dict.items():#pylint: disable=no-member
            self.loss_dict[key] += value.item()

        loss = sum(loss for loss in loss_dict.values()#pylint: disable=no-member
                   if not torch.isnan(loss))
        self._loss_backward(loss)

    def _get_loss_dict(self, loss_dict, predictions, ground_truth):#pylint: disable=unused-argument, no-self-use
        return loss_dict

    def log_end_epoch(self, nb_iterations):
        """String to display at the end of an epoch"""
        string = ""
        for key, value in self.loss_dict.items():
            string += f", {key}: {(value / nb_iterations):.3f}"
        self.loss_dict = defaultdict(int)
        return string

    def save(self, path, params=None):
        """Save the model for later re-use"""
        if params is None:
            params = {}

        params['options'] = self.options
        params['nets'] = [self.net.state_dict()]
        params['optimizers'] = {"detection": self.optimizer.state_dict()}
        torch.save(params, path)

    @classmethod
    def load(cls, path, options, device):
        """Load model from file"""
        checkpoint = torch.load(path, map_location=device)
        options.merge(checkpoint['options'])
        model = cls.create_model_from_checkpoint(checkpoint)
        for _, state_dict in checkpoint['optimizers'].items():
            model.optimizer.load_state_dict(state_dict)

        for _, state_dict in enumerate(checkpoint['nets']):
            model.net.load_state_dict(state_dict)
            model.net.to(device)

        return model

    @classmethod
    def create_model_from_checkpoint(cls, checkpoint):
        """Create a model using the parameters stored in the checkpoint"""
        return cls(checkpoint['options'])

    def train(self):
        """Call train on all nets"""
        self.net.train()

    def eval(self):
        """Call eval an all nets"""
        self.net.eval()

    @classmethod
    def update_arguments(cls, options):
        """Add parameters for the model"""
        options.parser.add_argument(
            '--learning-rate-detection', type=float, default=0.0002,
            dest="learning_rate_detection",
            help="Initial learning rate for detection net")
        options.parser.add_argument(
            '--num-classes', type=int, default=2,
            dest="num_classes",
            help="Number of classes in the dataset")

    def evaluate(self, epoch_index, data, paths):
        """Compute loss for model"""
        images = data["image"]
        boxes = data["boxes"]
        boxes_list = []
        for index in range(len(paths)):
            boxes_list.append({
                "boxes": boxes["boxes"][index],
                "labels": boxes["labels"][index]})
        predictions_list = self.net(images)
        losses = self.evaluation_loss(boxes_list, predictions_list)

        for loss, boxes, predictions, file_name in zip(
                losses, boxes_list, predictions_list, paths):
            file_name = os.path.basename(file_name)
            with open(os.path.join(
                    self.options.save_path,
                    f"{file_name}_epoch{epoch_index}.txt"
                    if epoch_index is not None
                    else f"{file_name}.txt"), "w") as file_:
                file_.write(f"{loss.item()}")
                file_.write("\n")
                file_.write(f"{boxes}")
                file_.write("\n")
                file_.write(f"{predictions}")
