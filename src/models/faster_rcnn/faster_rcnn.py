"""Extension of Faster-RCNN to get both the loss and the detections
at training time. Allows to add custom loss"""
from collections import OrderedDict

import torch
from torchvision.models.detection import FasterRCNN as FasterRCNNPytorch

from ..base_module import BaseModule

class FasterRCNN(FasterRCNNPytorch, BaseModule):
    """Extension of Faster-RCNN to get both the loss and the detections
    at training time. Allows to add custom loss"""
    def forward(self, images, targets=None):
        if self.training and targets is None:
            raise ValueError("In training mode, targets should be passed")
        original_image_sizes = [img.shape[-2:] for img in images]
        images, targets = self.transform(images, targets)
        features = self.backbone(images.tensors)
        if isinstance(features, torch.Tensor):
            features = OrderedDict([(0, features)])
        proposals, proposal_losses = self.rpn(images, features, targets)
        detections, detector_losses = self.roi_heads(
            features, proposals, images.image_sizes, targets)
        detections = self.transform.postprocess(
            detections, images.image_sizes, original_image_sizes)

        losses = {}
        losses.update(detector_losses)
        losses.update(proposal_losses)
        if self.training:
            return losses, detections

        return detections
