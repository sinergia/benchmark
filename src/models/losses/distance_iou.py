"""Distance IoU"""
import torch
from torch.nn.functional import pdist
from .iou import iou, _get_area
from .generalized_iou import _get_square_envelop

def _central_point(box):
    """Retrieve the central point of a box"""
    return torch.stack(box.chunk(2)).mean(dim=0)

def _diagonal_distance(box):
    """Compute the diagonal distance for a box"""
    return pdist(torch.stack(box.chunk(2)))

def _distance_penalty(box1, box2):
    """Compute the distance penalty term for the distance IoU"""
    return pdist(
        torch.stack((_central_point(box1), _central_point(box2)))
        ) / _diagonal_distance(_get_square_envelop(box1, box2))

def distance_iou(box1, box2, area1=None, area2=None, return_iou=False):
    """Compute the distance IoU score for the input boxes"""
    if area1 is None:
        area1 = _get_area(box1)
    if area2 is None:
        area2 = _get_area(box2)
    iou_ = iou(box1, box2, area1, area2)
    diou = iou_ - _distance_penalty(box1, box2)
    if return_iou:
        return diou, iou_
    return diou
