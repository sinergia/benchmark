"""Generalized intersection over union"""
import torch
from .iou import iou, _get_area

def _get_square_envelop(box1, box2):
    """Retrieve the closest square containing two boxes"""
    return torch.cat((torch.min(box1, box2)[:2], torch.max(box1, box2)[2:]))

def _convex_envelop_penalty(box1, box2, area1, area2, intersection_area):
    """Penalty for the generalized IoU"""
    convex_envelop = _get_square_envelop(box1, box2)
    convex_envelop_area = _get_area(convex_envelop)

    return (convex_envelop_area - area1 - area2 + intersection_area) / \
        convex_envelop_area

def generalized_iou(box1, box2, area1=None, area2=None, return_iou=False):
    """Compute the generalized intersection over union score
    for the input boxes"""
    if area1 is None:
        area1 = _get_area(box1)
    if area2 is None:
        area2 = _get_area(box2)
    iou_, intersection_area = iou(
        box1, box2, area1, area2, return_intersection=True)
    giou = iou_ - _convex_envelop_penalty(
        box1, box2, area1, area2, intersection_area)
    if return_iou:
        return giou, iou_
    return giou
