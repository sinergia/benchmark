"""Complete IoU"""
import math

from .distance_iou import distance_iou
from .iou import _get_area

def _aspect_ratio(box):
    """Compute the aspect ratio for a box"""
    dimensions = box.chunk(2)[1] - box.chunk(2)[0]
    return (dimensions[1] / dimensions[0]).atan()

def _aspect_ratio_penalty(box1, box2):
    """Compute the aspect ratio penalty"""
    return (2 * (_aspect_ratio(box2) - _aspect_ratio(box1)) / math.pi).pow(2)

def _complete_penalty(box1, box2, iou=None):
    ar_penalty = _aspect_ratio_penalty(box1, box2)
    if ar_penalty.item() > 0:
        return ar_penalty.pow(2) / (1 - iou + ar_penalty)
    #trick to return 0, while keeping the gradients
    return ar_penalty - ar_penalty

def complete_iou(box1, box2, area1=None, area2=None):
    """Compute the complete IoU score for the input boxes"""
    if area1 is None:
        area1 = _get_area(box1)
    if area2 is None:
        area2 = _get_area(box2)
    diou, iou = distance_iou(box1, box2, area1, area2, return_iou=True)
    return diou - _complete_penalty(box1, box2, iou)
