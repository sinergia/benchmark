"""Intersection over union for arbitrary lists of boxes"""
from networkx.algorithms.bipartite import from_biadjacency_matrix
from networkx.algorithms.matching import max_weight_matching
from scipy.sparse import csr_matrix
import torch
from torch.nn.functional import relu

def _get_areas(boxes):
    return relu(boxes[:, 2] - boxes[:, 0]) * relu(boxes[:, 3] - boxes[:, 1])

def _get_term(ious, dimension, areas, nb_boxes, diff_nb_boxes,
              area_normalization):
    if ious.size(1) == 0 or ious.size(0) == 0:
        return 0
    max_ious, _ = torch.max(ious, dimension)
    if area_normalization:
        max_ious = max_ious * areas / torch.sum(areas)
    return torch.sum(max_ious) / (nb_boxes + diff_nb_boxes)

def intersection(boxes_a, boxes_b):
    """Compute the intersection between any two boxes between two lists

    Parameters
    ----------
    boxes_a: torch.Tensor
        Bounding boxes, Shape: [A, 4].
    boxes_b: torch.Tensor
        Bounding boxes, Shape: [B, 4].

    Returns
    -------
    intersection matrix: torch.Tensor
        Intersection area for any two boxes, Shape: [A, B].
    """
    len_a = boxes_a.shape[0]
    len_b = boxes_b.shape[0]
    max_xy = torch.min(boxes_a[:, 2:].unsqueeze(1).expand(len_a, len_b, 2),
                       boxes_b[:, 2:].unsqueeze(0).expand(len_a, len_b, 2))
    min_xy = torch.max(boxes_a[:, :2].unsqueeze(1).expand(len_a, len_b, 2),
                       boxes_b[:, :2].unsqueeze(0).expand(len_a, len_b, 2))
    inter = torch.clamp(relu(max_xy - min_xy), min=0)
    return inter[:, :, 0] * inter[:, :, 1], len_a, len_b

def iou_matrix(boxes_a, boxes_b, areas_a, areas_b, return_union=False):
    """Compute the IoU of two sets of boxes.

    Parameters
    ----------
    boxes_a, boxes_b: torch.Tensor
        Bonding boxes, Shape: [nb_bounding_boxes, 4]

    Returns
    -------
    iou_matrix: torch.Tensor
        IoU for any two boxes
        Shape: [boxes_a.size(0), boxes_b.size(0)]
    """
    inter, len_a, len_b = intersection(boxes_a, boxes_b)
    area_a = areas_a.unsqueeze(1).expand_as(inter)
    area_b = areas_b.unsqueeze(0).expand_as(inter)
    union = area_a + area_b - inter
    if return_union:
        return inter / union, len_a, len_b, union
    return inter / union, len_a, len_b

def old_aux_multibox(boxes_a, boxes_b, matrix_func, term_getter,
                     area_normalization):
    """Generic function for turning a loss in multibox version"""
    # compute areas for each box
    areas_a = _get_areas(boxes_a)
    areas_b = _get_areas(boxes_b)

    ious, len_a, len_b = matrix_func(boxes_a, boxes_b, areas_a, areas_b)

    len_diff = abs(len_a - len_b)
    return 0.5 * (
        term_getter(ious, 0, areas_a, len_a, len_diff, area_normalization) +
        term_getter(ious, 1, areas_b, len_b, len_diff, area_normalization))

def aux_multibox(boxes_a, boxes_b, matrix_func, area_normalization, min_value):
    """Generic function for turning a loss in multibox version"""
    # compute areas for each box
    areas_a = _get_areas(boxes_a)
    areas_b = _get_areas(boxes_b)
    if area_normalization:
        sum_areas = sum(areas_a) + sum(areas_b)

    # compute IoU for each pair of boxes
    ious, len_a, len_b = matrix_func(
        boxes_a, boxes_b, areas_a, areas_b)

    matching = dict(max_weight_matching(from_biadjacency_matrix(csr_matrix(
        [[cell - min_value for cell in line] for line in ious.tolist()]))))

    multibox_loss = None
    for index in range(len(boxes_a)):
        if index in matching:
            index2 = matching[index] - len_a
            term = None
            if area_normalization:
                term = ious[index, index2] * (
                    areas_a[index] + areas_a[index2]) / sum_areas
            else:
                term = ious[index, index2]
            if multibox_loss is None:
                multibox_loss = term
            else:
                multibox_loss += term

    # arbitrary default zero value for multibox IoU
    if multibox_loss is None:
        try:
            multibox_loss = ious[0, 0]
        except IndexError:
            multibox_loss = torch.tensor(0.)#pylint: disable=not-callable

    return multibox_loss / max(len_a, len_b)

def old_multibox_iou(boxes_a, boxes_b, area_normalization=False):
    """Compute intersection over union for two lists of boxes"""
    return old_aux_multibox(boxes_a, boxes_b, iou_matrix, _get_term,
                            area_normalization)
