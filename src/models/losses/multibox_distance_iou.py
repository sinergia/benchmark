"""Distance IoU for multiple boxes"""
from .multibox_generalized_iou import _get_term, _get_convex_envelop
from .multibox_iou import old_aux_multibox, iou_matrix

def _distance_penalty(boxes_a, boxes_b, len_a, len_b, convex_envelop=None):
    center_x1 = (boxes_a[:, 2] + boxes_a[:, 0]).unsqueeze(1).expand(
        len_a, len_b)
    center_y1 = (boxes_a[:, 3] + boxes_a[:, 1]).unsqueeze(1).expand(
        len_a, len_b)
    center_x2 = (boxes_b[:, 2] + boxes_b[:, 0]).unsqueeze(0).expand(
        len_a, len_b)
    center_y2 = (boxes_b[:, 3] + boxes_b[:, 1]).unsqueeze(0).expand(
        len_a, len_b)
    inter_diag = (
        (center_x2 - center_x1).pow(2) + (center_y2 - center_y1).pow(2)) / 4
    if convex_envelop is None:
        convex_envelop = _get_convex_envelop(boxes_a, boxes_b, len_a, len_b)
    return inter_diag / (
        (convex_envelop[:, :, 0].pow(2)) + (convex_envelop[:, :, 1].pow(2)))

def distance_iou_matrix(boxes_a, boxes_b, areas_a, areas_b):
    """Compute the matrix of the box-to-box distance IoU"""
    iou, len_a, len_b = iou_matrix(boxes_a, boxes_b, areas_a, areas_b)
    return iou - _distance_penalty(boxes_a, boxes_b, len_a, len_b), len_a, len_b

def old_multibox_distance_iou(list1, list2, area_normalization=False):
    """Compute the distance IoU for two lists of boxes"""
    return old_aux_multibox(list1, list2, distance_iou_matrix,
                            _get_term, area_normalization)
