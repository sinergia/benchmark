"""Loss for Intersection over Union"""
import torch
from torch.nn import _reduction
from torch.nn.modules.loss import _Loss

from ...with_device import WithDeviceMixin
from .multibox_iou import old_multibox_iou

def _filter_boxes_for_class(input_, class_):
    """Keep only boxes from the given class"""
    # create mask for classes
    mask = input_['labels'].eq(class_).expand(
        # expand to the size of the boxes
        input_['boxes'].size(1), input_['boxes'].size(0)).transpose(0, -1)
    return input_['boxes'].masked_select(mask).view(-1, 4)

def iou_loss(input_, target, device, iou_func=old_multibox_iou,
             size_average=None, reduce_=None, reduction='mean',
             area_normalization=False):
    """Loss function for multi-class IoU

    Arguments
    ---------
    input_, target: list of dicts
        Prediction and reference value for a list of images
        For each image, we expect a dictionary containing:
        - boxes: the list of all detected boxes
        - labels: the list of labels for the boxes
        - scores: the list of scores for the boxes
    area_normalization: bool
        True to normalize the weight of a box in the multibox IoU metric by its
        relative area in the image compared to all the boxes
    """
    if size_average is not None or reduce_ is not None:
        reduction = _reduction.legacy_get_string(size_average, reduce_)
    per_image_iou = []
    for prediction, ground_truth in zip(input_, target):# for each image
        # get the list of found classes
        classes = torch.cat((prediction['labels'],
                             ground_truth['labels'])).unique()
        per_image_iou.append(sum([
            1 - iou_func(
                _filter_boxes_for_class(prediction, class_),
                _filter_boxes_for_class(ground_truth, class_),
                area_normalization
                ) for class_ in classes]) / len(classes))
    if reduction != 'none':
        if not per_image_iou:
            return torch.tensor(0., requires_grad=True).to(device)#pylint: disable=not-callable
        return sum(per_image_iou) / len(per_image_iou) if reduction == 'mean' \
            else sum(per_image_iou)
    return per_image_iou

class IoULoss(WithDeviceMixin, _Loss):
    """Loss module for IoU"""
    __constants__ = ['reduction']

    def __init__(self, iou_func=old_multibox_iou,
                 size_average=None, reduce_=None, reduction='mean',
                 area_normalization=False):
        super().__init__(size_average, reduce_, reduction)
        self.iou_func = iou_func
        self.area_normalization = area_normalization

    def forward(self, input_, target):# pylint: disable=arguments-differ
        return iou_loss(input_, target, self.device, reduction=self.reduction,
                        iou_func=self.iou_func,
                        area_normalization=self.area_normalization)
