"""Intersection over union"""
import torch
from torch.nn.functional import relu

def _get_area(box):
    """Compute the area of a box"""
    return relu(box[2] - box[0]) * relu(box[3] - box[1])

def _get_intersection(box1, box2):
    """Retrieve the intersection of two boxes or lists of boxes"""
    return torch.cat((torch.max(box1, box2)[:2], torch.min(box1, box2)[2:]))

def iou(box1, box2, area1=None, area2=None, return_intersection=False):
    """Return the intersection over union score for the input boxes

    Arguments:
    ----------
    box1, box2: tuple/list (x1, y1, x2, y2)
        Boxes to compare
    area1, area2: numbers
        Area of the boxes to compare
    return_intersection: bool
        True to return the area of the intersection, used in the generalized IoU

    Returns:
    --------
    iou: float
        Intersection over union score
    """
    intersection = _get_intersection(box1, box2)

    if area1 is None:
        area1 = _get_area(box1)
    if area2 is None:
        area2 = _get_area(box2)
    area_intersection = _get_area(intersection)

    iou_ = area_intersection / (area1 + area2 - area_intersection)

    if return_intersection:
        return iou_, area_intersection
    return iou_
