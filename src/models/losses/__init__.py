from .complete_iou import complete_iou
from .distance_iou import distance_iou
from .full_iou import full_iou
from .gan import GANLoss
from .generalized_iou import generalized_iou
from .hinge import HingeGANLoss
from .iou import iou
from .iou_loss import iou_loss, IoULoss
from .l1_box import l1_box
from .lsgan import LSGANLoss
from .multibox_complete_iou import old_multibox_complete_iou, \
    complete_iou_matrix
from .multibox_distance_iou import old_multibox_distance_iou, \
    distance_iou_matrix
from .multibox_full_iou import old_multibox_full_iou, full_iou_matrix
from .multibox_generalized_iou import old_multibox_generalized_iou, \
    generalized_iou_matrix
from .multibox_iou import aux_multibox, old_multibox_iou, iou_matrix
from .multibox_l1 import old_multibox_l1
from .segmentation import SegmentationLoss
from .wsgan import WSGANLoss

def multibox_iou(boxes1, boxes2, area_normalization=False):
    """Compute intersection over union for two lists of boxes"""
    return aux_multibox(boxes1, boxes2, iou_matrix, area_normalization, 0.)

def multibox_generalized_iou(boxes1, boxes2, area_normalization=False):
    """Compute generalized IoU for two lists of boxes"""
    return aux_multibox(boxes1, boxes2, generalized_iou_matrix,
                        area_normalization, -1.)

def multibox_distance_iou(boxes1, boxes2, area_normalization=False):
    """Compute distance IoU for two lists of boxes"""
    return aux_multibox(boxes1, boxes2, distance_iou_matrix,
                        area_normalization, -1.)

def multibox_complete_iou(boxes1, boxes2, area_normalization=False):
    """Compute complete IoU for two lists of boxes"""
    return aux_multibox(boxes1, boxes2, complete_iou_matrix,
                        area_normalization, -2.)

def multibox_full_iou(boxes1, boxes2, area_normalization=False):
    """Compute full IoU for two lists of boxes"""
    return aux_multibox(boxes1, boxes2, full_iou_matrix,
                        area_normalization, -3.)

AVAILABLE_IOU = {
    "simple": multibox_iou,
    "generalized": multibox_generalized_iou,
    "distance": multibox_distance_iou,
    "complete": multibox_complete_iou,
    "full": multibox_full_iou,
    "old_simple": old_multibox_iou,
    "old_generalized": old_multibox_generalized_iou,
    "old_distance": old_multibox_distance_iou,
    "old_complete": old_multibox_complete_iou,
    "old_full": old_multibox_full_iou,
    #"old_L1": old_multibox_l1,
    }
