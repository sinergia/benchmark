"""Full IoU: combination of complete IoU and generalized IoU"""
from .complete_iou import _complete_penalty
from .distance_iou import _distance_penalty
from .generalized_iou import generalized_iou
from .iou import _get_area

def full_iou(box1, box2, area1=None, area2=None):
    """Full IoU: combination of complete IoU and generalized IoU"""
    if area1 is None:
        area1 = _get_area(box1)
    if area2 is None:
        area2 = _get_area(box2)
    giou, iou = generalized_iou(box1, box2, area1, area2, return_iou=True)
    return giou - _distance_penalty(box1, box2) - _complete_penalty(
        box1, box2, iou)
