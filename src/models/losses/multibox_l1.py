"""L1 loss for multiple boxes"""
import torch

from .l1_box import l1_box
from .multibox_iou import old_aux_multibox

def _get_term(losses, dimension, areas, nb_boxes, diff_nb_boxes,
              area_normalization):
    min_loss, _ = torch.min(losses, dimension)
    if area_normalization:
        min_loss = min_loss * areas / torch.sum(areas)
    return torch.sum(min_loss) * (1 + diff_nb_boxes) / nb_boxes

def old_multibox_l1(list1, list2, area_normalization=False):
    """Compute generalized intersection over union for two lists of boxes"""
    return old_aux_multibox(list1, list2, l1_box,
                            _get_term, area_normalization)
