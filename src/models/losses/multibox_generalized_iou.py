"""Generalized intersection over union for arbitrary lists of boxes"""
import torch
from torch.nn.functional import relu
from .multibox_iou import old_aux_multibox, iou_matrix

def _get_term(gious, dimension, areas, nb_boxes, diff_nb_boxes,
              area_normalization):
    max_gious, _ = torch.max(gious, dimension)
    if area_normalization:
        max_gious = max_gious * areas / torch.sum(areas)
    ponderated_max_giou = relu(max_gious) / (nb_boxes + diff_nb_boxes) - \
        relu(-max_gious) * (1 + diff_nb_boxes) / nb_boxes
    return torch.sum(ponderated_max_giou)

def _get_convex_envelop(boxes_a, boxes_b, len_a, len_b):
    max_xy = torch.max(boxes_a[:, 2:].unsqueeze(1).expand(len_a, len_b, 2),
                       boxes_b[:, 2:].unsqueeze(0).expand(len_a, len_b, 2))
    min_xy = torch.min(boxes_a[:, :2].unsqueeze(1).expand(len_a, len_b, 2),
                       boxes_b[:, :2].unsqueeze(0).expand(len_a, len_b, 2))
    return torch.clamp(relu(max_xy - min_xy), min=0)

def _giou_penalty(boxes_a, boxes_b, union, len_a, len_b, return_convex=False):
    convex_envelop = _get_convex_envelop(boxes_a, boxes_b, len_a, len_b)
    c_area = convex_envelop[:, :, 0] * convex_envelop[:, :, 1]
    if return_convex:
        return (c_area - union) / c_area, convex_envelop
    return (c_area - union) / c_area

def generalized_iou_matrix(boxes_a, boxes_b, areas_a, areas_b):
    """Compute the matrix of the box-to-box generalized IoU"""
    iou, len_a, len_b, union = iou_matrix(
        boxes_a, boxes_b, areas_a, areas_b, True)
    return iou - _giou_penalty(
        boxes_a, boxes_b, union, len_a, len_b), len_a, len_b

def old_multibox_generalized_iou(list1, list2, area_normalization=False):
    """Compute generalized intersection over union for two lists of boxes"""
    return old_aux_multibox(list1, list2, generalized_iou_matrix, _get_term,
                            area_normalization)
