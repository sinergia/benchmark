"""L1 loss for boxes"""
import torch
from torch.nn.functional import l1_loss

def l1_box(box1, box2, *args):# pylint: disable=unused-argument
    """Use  the L1 loss to compute a loss function for boxes"""
    return l1_loss(torch.FloatTensor(box1), torch.FloatTensor(box2))
