"""Full IoU for multiple boxes"""
from .multibox_generalized_iou import _get_term, _giou_penalty
from .multibox_complete_iou import _complete_penalty
from .multibox_iou import old_aux_multibox, iou_matrix

def full_iou_matrix(boxes_a, boxes_b, areas_a, areas_b):
    """Compute the matrix of the box-to-box full IoU"""
    iou, len_a, len_b, union = iou_matrix(boxes_a, boxes_b, areas_a, areas_b,
                                          return_union=True)
    g_p, convex_envelop = _giou_penalty(boxes_a, boxes_b, union, len_a, len_b,
                                        return_convex=True)
    return iou - g_p - _complete_penalty(
        boxes_a, boxes_b, iou, len_a, len_b, convex_envelop), len_a, len_b

def old_multibox_full_iou(list1, list2, area_normalization=False):
    """Compute the full IoU for two lists of boxes"""
    return old_aux_multibox(list1, list2, full_iou_matrix, _get_term,
                            area_normalization)
