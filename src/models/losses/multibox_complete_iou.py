"""Complete IoU for multiple boxes"""
import math
import torch

from .multibox_distance_iou import _distance_penalty
from .multibox_generalized_iou import _get_term
from .multibox_iou import old_aux_multibox, iou_matrix

def _complete_penalty(boxes_a, boxes_b, iou, len_a, len_b, convex_envelop=None):
    width_a = (boxes_a[:, 2] - boxes_a[:, 0]).unsqueeze(1).expand(len_a, len_b)
    height_a = (boxes_a[:, 3] - boxes_a[:, 1]).unsqueeze(1).expand(len_a, len_b)
    width_b = (boxes_b[:, 2] - boxes_b[:, 0]).unsqueeze(0).expand(len_a, len_b)
    height_b = (boxes_b[:, 3] - boxes_b[:, 1]).unsqueeze(0).expand(len_a, len_b)
    aspect_ratio_term = (2 * (
        torch.atan2(width_a, height_a) - torch.atan2(width_b, height_b)) /
                         math.pi).pow(2)
    penalty = _distance_penalty(boxes_a, boxes_b, len_a, len_b, convex_envelop)
    penalty.add_(aspect_ratio_term.pow(2) / (1 + 1e-7 - iou + aspect_ratio_term))
    return penalty

def complete_iou_matrix(boxes_a, boxes_b, areas_a, areas_b):
    """Compute the matrix of the box-to-box complete IoU"""
    iou, len_a, len_b = iou_matrix(boxes_a, boxes_b, areas_a, areas_b)
    return iou - _complete_penalty(boxes_a, boxes_b, iou, len_a, len_b), \
        len_a, len_b

def old_multibox_complete_iou(list1, list2, area_normalization=False):
    """Compute the complete IoU for two lists of boxes"""
    return old_aux_multibox(list1, list2, complete_iou_matrix,
                            _get_term, area_normalization)
