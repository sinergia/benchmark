"""Dataset for Coco Object detection"""
import json
import os

from .bbox import BboxesMixin
from .image_folder_with_annotations import ImageFolderWithAnnotationsDataset

class CocoBboxDataset(BboxesMixin, ImageFolderWithAnnotationsDataset):#pylint: disable=too-few-public-methods
    """Dataset for Coco Object detection"""
    def __init__(self, *args, **kwargs):
        self.transform_params["bboxes"] = "coco"
        kwargs["key_getter"] = os.path.basename
        super().__init__(*args, **kwargs)
        with open(self.annotation_path, 'r') as file_:
            dataset = json.load(file_)

        image_ids = {image["id"]: image["file_name"]
                     for image in dataset["images"]}
        self.bboxes = {}
        for annotation in dataset["annotations"]:
            image_file_name = image_ids[annotation["image_id"]]
            if image_file_name not in self.bboxes.keys():
                self.bboxes[image_file_name] = [{
                    "bbox": annotation["bbox"],
                    "label": annotation["category_id"]}]
            else:
                self.bboxes[image_file_name].append({
                    "bbox": annotation["bbox"],
                    "label": annotation["category_id"]})
        self._aggregate_boxes()
