# -*- coding: utf-8 -*-
"""Dataset corresponding to a folder of images"""
import os
from PIL import Image, ImageFile
import numpy as np

from .base import BaseDataset
from .utils import make_dataset, get_transform

ImageFile.LOAD_TRUNCATED_IMAGES = True

class ImageFolderDataset(BaseDataset):
    """Class representing a dataset with image and target domain"""
    transform_params = {}

    def __init__(self, options, folder=None, transform=None, **kwargs):# pylint: disable=unused-argument
        super().__init__(options)
        # create path to folder
        self.folder_path = os.path.join(
            options.dataroot, folder if folder is not None else options.source)
        # load images from 'folder'
        self.image_paths = make_dataset(
            self.folder_path, options.max_dataset_size, options.recursive)
        # get the size of the dataset
        self.size = len(self.image_paths)
        self.transform = get_transform(options, transform,
                                       **self.transform_params)

    def _get_data(self, index):
        # make sure index is within the range
        image_path = self.image_paths[index % self.size]
        image = np.array(Image.open(image_path))
        return image_path, {"image": image}

    def __getitem__(self, index):
        """Return one image from folder.

        Parameters
        ----------
        index: int
            Index of image

        Returns
        -------
        data: tuple
            (dict, file_name)
        """
        image_path, data = self._get_data(index)
        # apply image transformation
        transformed_data = self.transform(**data)
        self._to_device(transformed_data)
        return (transformed_data, image_path)

    def _to_device(self, data):
        data["image"] = data["image"].to(self.device)

    def __len__(self):
        """Return the total number of images in the dataset.
        """
        return self.size
