"""Dataset for CityPersons"""
import os
import json

from .bbox import BboxesMixin
from .image_folder_with_annotations import ImageFolderWithAnnotationsDataset

LABELS = [
    "ignore",
    "pedestrian",
    "person (other)",
    "person group",
    "rider",
    "sitting person",
]

def _reformat_boxes(bbox, width, height):
    # avoid negative bottom
    if bbox[0] < 0:
        bbox[2] += bbox[0]
        bbox[0] = 0
    # avoid negative left
    if bbox[1] < 0:
        bbox[3] += bbox[1]
        bbox[1] = 0

    # avoid box too wide
    if bbox[0] + bbox[2] > width:
        bbox[2] = width - bbox[0]

    # avoid box too tall
    if bbox[1] + bbox[3] > height:
        bbox[3] = height - bbox[1]

    return bbox

class CityPersonsDataset(BboxesMixin, ImageFolderWithAnnotationsDataset):#pylint: disable=too-few-public-methods
    """Dataset for CityPersons"""
    def __init__(self, *args, **kwargs):
        max_dataset_size = args[0].max_dataset_size
        args[0].max_dataset_size = float("inf")
        self.transform_params["bboxes"] = "coco"
        args[0].num_classes = len(LABELS)
        super().__init__(*args, **kwargs)
        self.options.max_dataset_size = max_dataset_size
        bbox_property = "bboxVis" if self.options.visible_only else "bbox"
        index = 0
        for directory_path, _, file_names in os.walk(self.annotation_path,
                                                     followlinks=True):
            for file_name in file_names:
                image_file_name = os.path.join(
                    self.folder_path, file_name.replace(
                        "gtBboxCityPersons.json", "leftImg8bit.png"))
                if index < max_dataset_size and \
                        image_file_name in self.image_paths:
                    index += 1
                    with open(os.path.join(directory_path, file_name)) as file_:
                        data = json.load(file_)
                    self.bboxes[image_file_name] = [
                        {"bbox": _reformat_boxes(
                            box[bbox_property], data["imgWidth"],
                            data["imgHeight"]),
                         "label": LABELS.index(box["label"])}
                        for box in data["objects"]
                        # avoid empty bounding boxes
                        if box[bbox_property][2] > 0 and \
                            box[bbox_property][3] > 0
                        ]
                    if not self.bboxes[image_file_name]:
                        del self.bboxes[image_file_name]
        self.image_paths = list(self.bboxes.keys())
        self.size = len(self.image_paths)
        self._aggregate_boxes()

    @classmethod
    def update_arguments(cls, options):
        super().update_arguments(options)
        options.parser.add_argument(
            '--visible-only', action='store_true', dest="visible_only",
            help='Take boxes for the visible part of the objects only')
