"""Dataset for INIT"""
from .bbox import BboxesMixin
from .image_folder_with_annotations import ImageFolderWithAnnotationsDataset

class INITDataset(BboxesMixin, ImageFolderWithAnnotationsDataset):#pylint: disable=too-few-public-methods
    """Dataset for INIT"""
    def __init__(self, *args, **kwargs):
        args[0].recursive = True
        self.transform_params["bboxes"] = "pascal_voc"
        super().__init__(*args, **kwargs)
        self.bboxes = {}
        with open(self.annotation_path, 'r') as file_:
            while file_.readline():
                # first line is the index
                # second is the path to the image
                path = file_.readline()[:-1]
                self.bboxes[path] = []
                # third is the dimensions of the image
                file_.readline()
                # fourth is 0
                file_.readline()
                # fifth is the number of bounding boxes
                nb_bounding_boxes = int(file_.readline()[:-1])
                # subsequent lines are `label x1 y1 x2 y2` for the bboxes
                for _ in range(nb_bounding_boxes):
                    infos = file_.readline()[:-1].split()
                    self.bboxes[path].append({
                        "label": int(infos[0]),
                        "bbox": [float(coord) for coord in infos[1:]]})
                # end with 2 empty lines
                file_.readline()
                file_.readline()
        self._aggregate_boxes()
