"""Dataset for Pascal VOC Object detection"""
from xml.etree import ElementTree
import os

from .bbox import BboxesMixin
from .image_folder_with_annotations import ImageFolderWithAnnotationsDataset

LABELS = [
    "person", "bird", "cat", "cow", "dog", "horse", "sheep", "aeroplane",
    "bicycle", "boat", "bus", "car", "motorbike", "train", "bottle", "chair",
    "diningtable", "pottedplant", "sofa", "tvmonitor",]

class PascalVocBboxDataset(BboxesMixin, ImageFolderWithAnnotationsDataset):#pylint: disable=too-few-public-methods
    """Dataset for Pascal VOC Object detection"""
    def __init__(self, *args, **kwargs):
        self.transform_params["bboxes"] = "pascal_voc"
        args[0].num_classes = len(LABELS)
        super().__init__(*args, **kwargs)
        self.bboxes = {}
        for file_name in self.image_paths:
            tree = ElementTree.parse(
                os.path.join(
                    self.annotation_path,
                    os.path.basename(file_name).replace('jpg', 'xml')))

            self.bboxes[file_name] = [
                {"bbox": [
                    [int(box.find("xmin").text), int(box.find("ymin").text),
                     int(box.find("xmax").text), int(box.find("ymax").text)]
                    for box in annotation.findall("bndbox")][0],
                 "label": LABELS.index(annotation.find("name").text)}
                for annotation in tree.findall("object")]
        self._aggregate_boxes()
