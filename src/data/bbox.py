"""Dataset with bounding boxes"""

def _identity(value):
    return value

class BboxesMixin():#pylint: disable=too-few-public-methods
    """Mixin for bonding boxes"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_getter = kwargs.get("key_getter", _identity)
        self.bboxes = {}

    def _aggregate_boxes(self):
        self.bboxes = {
            key: {
                "boxes": [bbox["bbox"] for bbox in value],
                "labels": [bbox["label"] for bbox in value]
                }
            for key, value in self.bboxes.items()}

    def _get_annotation(self, index, image_path, data):
        super()._get_annotation(index, image_path, data)
        boxes = self.bboxes[self.key_getter(image_path)]
        data["bboxes"] = boxes["boxes"]
        data["labels"] = boxes["labels"]

    def _to_device(self, data):
        super()._to_device(data)
        data["boxes"] = {
            "boxes": data["bboxes"].to(self.device),
            "labels": data["labels"].to(self.device)}#, dtype=torch.int64)

    @classmethod
    def update_arguments(cls, options):
        super().update_arguments(options)
        options.parser.add_argument(
            '--min-visibility', type=float, default=0., dest="min_visibility",
            help='Minimum fraction of area for a bounding box to retain this ' +
            'box in list for the tranformed image')
        options.parser.add_argument(
            '--min-area', type=float, default=0., dest="min_area",
            help='Minimum area of a bounding box. All bounding boxes whose ' +
            'visible area in pixels is less than this value will be removed' +
            ' in the transformed images')
