#! /bin/bash

cd /sinergia/benchmark
./test.py --model UNIT -v --name UNIT_kitty_cityscapes --batch-size 10 --source kitty/test --target cityscapes/test --data-root ..
