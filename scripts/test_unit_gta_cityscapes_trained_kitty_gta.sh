#! /bin/bash

cd /sinergia/benchmark
./test.py --model UNIT -v --name UNIT_gta_cityscapes --batch-size 10 --target kitty/test --source gta5/images/test --data-root .. --save-path UNIT_gta_cityscapes_results/test/gta_kitty
