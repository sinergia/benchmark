#! /bin/bash

cd /sinergia/benchmark
./test.py --model UNIT -v --name UNIT_gta_cityscapes --batch-size 10 --source kitty/test --target cityscapes/test --data-root .. --save-path UNIT_gta_cityscapes_results/test/kitty_cityscapes
