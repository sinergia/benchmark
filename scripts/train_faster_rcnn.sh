#! /bin/bash

cd /sinergia/benchmark
./train.py --dataset-type citypersons --model FasterRCNN -e 100 --save --evaluate --evaluation-frequency 1 --save-frequency 1 --name FasterRCNN_citypersons --max-dataset-size 2000 --normalize --verbose --source-annotation cityscapes/gtBboxCityPersons/train --source cityscapes --data-root .. --num-threads 4 --visible-only
