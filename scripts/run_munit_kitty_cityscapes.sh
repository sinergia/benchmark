#! /bin/bash

cd /sinergia/benchmark
./train.py --dataset-type SourceTarget --model MUNIT -e 100 --save -v --evaluate --evaluation-frequency 1 --save-frequency 1 --name MUNIT_kitty_cityscapes --source kitty --target cityscapes --data-root .. --max-dataset-size 2000 --use-mixed-precision
