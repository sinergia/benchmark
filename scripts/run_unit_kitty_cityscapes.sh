#! /bin/bash

cd /sinergia/benchmark
./train.py --dataset-type SourceTarget --model UNIT -e 100 --save -v --evaluate --evaluation-frequency 1 --save-frequency 1 --name UNIT_kitty_cityscapes --batch-size 10 --source kitty --target cityscapes --max-dataset-size 2000 --data-root .. --use-mixed-precision
