#! /bin/bash

cd /sinergia/benchmark
./train.py --dataset-type MultiDomain --model DRITObjectDetection -e 100 --nb-pretraining-epochs 100 --save --evaluate --evaluation-frequency 1 --save-frequency 1 --name DRITObjectDetection_cityscapes_gta --domain-folders cityscapes,gta5/images -d cityscapes,gta --data-root .. --num-threads 4 --max-dataset-size 2000 --domain-annotations citypersons, --domain-annotation-folders gtBboxCityPersons/train, --with-annotations --pool-size=2 --input-size 216 --normalize --batch-size 2 --verbose --use-mixed-precision
