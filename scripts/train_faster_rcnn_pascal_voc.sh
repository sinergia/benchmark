#! /bin/bash

cd /sinergia/benchmark
./train.py --dataset-type pascal_voc --model FasterRCNN -e 100 --save --evaluate --evaluation-frequency 1 --save-frequency 10 --name FasterRCNN_Pascal_VOC --max-dataset-size 3000 --normalize --verbose --source-annotation Annotations --source JPEGImages --data-root ../VOCdevkit/VOC2012/ --num-threads 4
