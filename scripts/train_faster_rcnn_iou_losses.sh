#! /bin/bash

cd /sinergia/benchmark
for loss in "generalized" "distance" "complete" "full" "old_generalized" "old_distance" "old_complete" "old_full"
do
    ./train.py --dataset-type citypersons --model FasterRCNNObjectDetection -e 100 --save --evaluate --evaluation-frequency 2 --save-frequency 1 --name "FasterRCNN_citypersons_${loss}_iou" --max-dataset-size 2000 --normalize --verbose --source-annotation cityscapes/gtBboxCityPersons/train --source cityscapes --data-root .. --num-threads 4 --iou-func "$loss" --visible-only
done
