\subsection{Object detection}

    Standard models in objection are fully supervized methods, needing large
    datasets with instance-level annotations for training.
    R-CNN~\cite{girshick_rich_2014}, Fast-RCNN~\cite{girshick_fast_2015} and
    Faster-RCNN~\cite{ren_faster_2016} are based on one step of region proposals
    then one step of classification. YOLO~\cite{redmon_you_2016-1},
    SSD~\cite{liu_ssd:_2016} and RetinaNet\cite{lin_focal_2018} rely on
    splitting an image according to a grid, then detecting and classifying the
    instances in one pass.
    \g{Add weakly supervized methods ?}

    However, Wilber et al. have shown in~\cite{wilber_bam!_2017} that these
    models, once trained on a dataset, do not generalize well to images from a
    different domain, e.g. from a dataset of comics images.

\subsection{Style-transfer}

    Isola et al. propose Pix2pix in~\cite{isola_image--image_2017}, the first
    unified model for style-tranfer. However, their work is based on the
    existence of paired data, which we cannot rely on.

    Unsupervised style-transfer models rely on the translation preserving
    certain properties of the source image.
    Zhu et al.~\cite{zhu_unpaired_2017} instroduced the cycle-consistency loss
    in their CycleGAN, concurrently with DualGAN~\cite{yi_dualgan:_2017}.
    Liu et al.~\cite{liu_unsupervised_2017} proposed the UNIT framework,
    assuming that there exists a latent space encoding the content of an image
    which is shared across two domains.
    \cite{gan_unpaired_2018} is similar in its use of a third ``auxiliary''
    space for domain information. Further attempts at disentanglement
    introduce separate style latent spaces~\cite{ferrari_multimodal_2018},
    domain discriminators~\cite{lee_diverse_2018, lee_dritplus_2019} or
    classifiers~\cite{lin_exploring_2019}, maximizing the mutual information
    between an image and its latent code~\cite{chen_infogan:_2016}, ...

    While CycleGAN and UNIT can only translate from one domain to a unique other,
    we wish, in perspective of the diversity of comics styles, to be able to
    handle multiple domains at once.
    ComboGAN~\cite{anoosheh_combogan:_2018} extends CycleGAN to support multiple
    domains at once by splitting a generator into encoder plus decoder.
    StarGAN~\cite{choi_stargan:_2018} uses a mask of domains to be able to
    translate from and to any combination of domains.

    \cite{perera_in2i:_2018} focuses on generating an image from multiple
    semantically related images across several domains, while severa other works
    focus  on generating multiple diverse images from a same
    input~\cite{zhu_toward_2018, lee_dritplus_2019, almahairi_augmented_2018}.
    Some also introduced using a pyramid of networks in a
    coarse-fine-fashion~\cite{katzir_cross-domain_2019, shaham_singan:_2019,
    li_unsupervised_2018} in order to speed up the training and improve the
    performance, using a single image for training~\cite{shaham_singan:_2019},
    or generating high-quality images~\cite{junginger_unpaired_2018}.

    Comixify~\cite{pesko_comixify:_2018} and CartoonGAN~\cite{chen_cartoongan:_2018}
    translate to comics images, while CariGAN~\cite{cao_carigans:_2018} handles
    caricatures.
    Mao et al.~\cite{mao_unpaired_2018} use regularizers to improve the quality
    of the generated images, Ulyanov et al.~\cite{ulyanov_instance_2016,
    ulyanov_improved_2017, ulyanov_texture_nodate} also improved the image
    generation through a new normalization layer. Zhao et al.\cite{zhao_loss_2015},
    as weel as Johnson et al.~\cite{johnson_perceptual_nodate} show that the use
    of more specific and suitable loss functions for training the GANs leads to
    better results, while Chen et al.~\cite{chen_quality-aware_2019} designed a quality-aware loss function.

    Many more have focused on a more specific task and have adapted one of the
    aforementionned models for their needs, leveraging the presence of edges
    or facial landmarks to improve face-generation~\cite{dou_asymmetric_2019,
    di_gp-gan:_2018}, segmentation masks~\cite{tomei_art2real:_2018},
    introducing attention mecanisms~\cite{ma_da-gan:_2018,
    tang_attention-guided_2019, kim_u-gat-it:_2019} or a multi-scale
    architecture~\cite{zhu_unpaired_2017}.

    Object detection being close to segmentation tasks, we focused more
    specifically on applications using segmentation, like
    AugGAN~\cite{ferrari_auggan:_2018} which generates a segmentation along
    with a transfered image, and using a segmentation loss, or medical
    applications like X-Ray image segmentation~\cite{frangi_task_2018,
    zhang_translating_2018}, MR image segmentation~\cite{liu_susan:_2019} or
    detecting surgical instruments~\cite{lee_davincigan:_nodate}. We were also
    interested in instance-focused works, usually models translating both a
    whole image and its instances, like InstaGAN~\cite{mo_instagan:_2019} or
    INIT~\cite{shen_towards_2019}.


\subsection{Intersection over union}

    \emph{Intersection over union} (IoU) is the metric chosen in many, if not
    all, tasks on object detection and segmentation. However, the losses used
    to train object detection and segmentation models rely usually on a $l_n$
    norm or other simple functions, which suffer of various issues among which
    class imbalance~\cite{bebis_optimizing_2016}, weakness for scale
    variations~\cite{yu_unitbox:_2016}.

    Rhaman et al.~\cite{bebis_optimizing_2016} propose to optimize a
    pixel-based approximation of the IoU function for segmentation models.
    Nagendar et al.~\cite{nagendar_neuroiou:_nodate} proposes to approximate
    the IoU using a neural network, to guarantee differentiability. However,
    applying these methods to object detection would be counter-productive, as
    we would need to create a segmentation mask from the bounding boxes, and
    deal the issue of overlapping bounding boxes, because this does not occur
    in segmentation tasks.

    Yu et al.~\cite{yu_unitbox:_2016} illustrate the lack of robustness of the
    $l_2$ norm when faced with scale variations, and propose to use the same
    metric for evaluation and training. They propose an IoU layer improving
    the object detection. Subsequent works~\cite{rezatofighi_generalized_2019,
    zheng_distance-iou_2019} propose to add a penalty-term to the vanilla IoU
    function, in order to account for its lack of differentiation between
    non-overlapping cases. However, these works define their metrics only for
    comparing two boxes. As for KITTI~\cite{geiger_vision_2013},
    INIT~\cite{shen_towards_2019}, and other public challenges and datasets,
    their method for computing the IoU is not disclosed, even though this
    metric is used for evaluation, and lists of boxes is the usual output
    format for object detection models.
    The provided evaluation source codes seem to do a greedy search through the
    lists to find the corresponding boxes.

    To the best of our knowledge, no formalization of a metric comparing two
    lists of boxes, namely predicted bounding boxes vs ground-truth, has been
    made available.
