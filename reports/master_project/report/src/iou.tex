In the following, with $f$ a function, we define its associated loss function by $1 - f$.

\subsection{Intersection over Union for two boxes}

    Intersection over Union (IoU), also referred to as the Jaccard distance, is
    a common evaluation metric for object detection models.

    With $A$ and $B$ two shapes, their IoU is given by
    $\iou(A, B) = \frac{A \cap B}{A \cup B} =
        \frac{\area(A \cap B)}{\area(A) + \area(B) - \area(A \cap B)}$.
    With boxes, we can define $A$ and $B$ by the coordinates of their bottom-left
    and top-right corner: $A = (x_{A, 1}, y_{A, 1}, x_{A, 2}, y_{A, 2})$,
    $B = (x_{B, 1}, y_{B, 1}, x_{B, 2}, y_{B, 2})$. In this case, we can easily
    compute the intersection and areas of the shapes:
    $$A \cap B = \prt{\max(x_{A, 1}, x_{B, 1}), \max(y_{A, 1}, y_{B, 1}),
                     \min(x_{A, 2}, x_{B, 2}), \min(y_{A, 2}, y_{B, 2})}$$ and
    $\area(X) = \relu\prt{x_{X, 2} - x_{X, 1}} \times \relu\prt{y_{X, 2} - y_{X, 1}}$.

    IoU has the following desirable properties:
    \begin{itemize}
        \item $\forall A, B, 0 \leq \iou(A, B) \leq 1$
        \item IoU is concave, and $\forall A, B \mapsto \iou(A, B)$ has a
            unique maximum different from 0, reached in $B = A$.
        \item IoU is independent of the scale
        \item IoU is independent of the way the bounding boxes are defined
        \item IoU is sub-differentiable~\cite{yu_unitbox:_2016},
            which allow us to use it to train our models
    \end{itemize}

    However, as soon as $A$ and $B$ are non-overlapping, their IoU is null
    , and IoU cannot provide any moving gradient.

    To overcome this issue, Rezatofighi et al.~\cite{rezatofighi_generalized_2019},
    then Zheng et al.~\cite{zheng_distance-iou_2019} proposed IoU-based losses
    keeping the same properties as IoU, except for its bounds:
    \begin{itemize}
        \item Generalized IoU:
            $\giou(A, B) = \iou(A, B) -
            \frac{\convex(A \cup B) \cap (A \cup B)}{\convex(A \cup B)}$
            where $\convex(X)$ describes the convex hull of the shape X.
            $\forall A, B, -1 \leq \giou(A, B) \leq 1$
        \item Distance IoU:
            $\diou(A, B) = \iou(A, B) -
            \frac{\nor{c_A - c_B}}{diag(\convex(A \cup B))}$,
            where $diag(X)$ denotes the length of the diagonal of $X$,
            and $c_X$ the center of the box $X$.
            $\forall A, B, -1 \leq \diou(A, B) \leq 1$
        \item Complete IoU: $\ciou(A, B) = \diou(A, B) -
            \frac{v^2(A, B)}{(1 - IoU(A, B) + v(A, B)}$, with \\
            $v(A, B) = \prt{\frac{2}{\pi}
                \prt{arctan\prt{\frac{w_A}{h_A}} -
                     arctan\prt{\frac{w_B}{h_B}}}}^2$,
            where $w_X$ is the width of $X$ and $h_X$ its height.
            $\forall A, B, -2 \leq \ciou(A, B) \leq 1$
    \end{itemize}

    Seeing that the Distance IoU does not reuse the penalty term of the
    Generalized IoU, we define Full IoU as the combination of both the
    Complete IoU and the Generalized IoU:
    $$\fiou(A, B) = \iou(A, B) -
        \frac{\convex(A \cup B) \cap (A \cup B)}{\convex(A \cup B)} -
        \frac{\nor{c_A - c_B}}{diag(\convex(A \cup B))} -
        \frac{v^2(A, B)}{(1 - IoU(A, B) + v(A, B)}$$
        with the same notations as above.

\subsection{Metrics and losses on lists of boxes}

    The above metrics are only defined for comparing two boxes.
    However, we usually have to compare a list of predicted boxes to
    the list of ground-truth boxes.

    Methods have been devised and implemented (eg for the evaluation
    of the COCO results) for generalizing the IoU metric.
    However, these methods seem to rely on greedy algorithms,
    and are not formalized.

    \subsubsection{Naive solution}

        The issue with scaling to arbitrary lists of boxes resides in the
        mapping of a box in the first list to a box in the second. Indeed,
        the first ideas would be sorting the list of boxes, which would need
        us to define a sub-gradient of the sort function, or taking for each
        box of a list the box of the other list matching it the most closely.
        However, with $P$ the list of predicted boxes et $GT$ the list of
        ground-truth boxes, both $\sum[p \in P]{\max[g \in GT]{\iou(p, g)}}$
        and $\sum[g \in GT]{\max[p \in P]{\iou(p, g)}}$ suffer of the
        degenerescence of the size of the lists:
        \begin{enumerate}
            \item in the first case, predicting no boxes at all is not worse
                than predicting only non-overlapping boxes, and is even better
                if we use the generalized IoU
            \item in the second case, predicting each and every box in the
                image achieves the highest score
        \end{enumerate}

        Also, we might want to lead our model towards predicting the same number
        of boxes as in the ground truth. Thus we might want to decrease the
        total metric based on the difference of length between the two lists.

        We thus propose the following formulas to compute the global
        intersection over union score of two lists of boxes:
        \begin{itemize}
            \item $\miou(A, B) = \frac{1}{2} \prt{
                    \frac{1}{\card{A} + \abs{\card{B} - \card{A}}}
                    \sum[a \in A]{\max[b \in B]{\iou(a, b)}} +
                    \frac{1}{\card{B} + \abs{\card{B} - \card{A}}}
                    \sum[a \in A]{\max[b \in B]{\iou(a, b)}}}$ for IoU
            \item $\miou(f, A, B) = \frac{1}{2} \prt{
                    \sum[a \in A]{\MIoU(f, a, B)} +
                    \sum[b \in B]{\MIoU(f, b, A)}}$
                for other IoU-based metrics, where
                $$\MIoU(f, a, B) = \prt{
                    \frac{\ind{\Miou(f, a, B) > 0}}{\card{A} + \abs{\card{B} - \card{A}}} +
                    \ind{\Miou(f, a, B) < 0} \times \frac{\abs{\card{B} - \card{A}} + 1}{\card{A}}}
                    \Miou(f, a, B)$$
                $$\Miou(f, a, B) = \max[b \in B]{f(a, b)}$$
                and $f$ is the Iou-based metric to use to compare two boxes.
        \end{itemize}

        These metrics can be implemented as combinations of $\max{}$, $\min{}$,
        $\relu$ and the underlying IoU-base metric, thus are subdifferentiable,
        and can be used as losses for neural networks. A Pytorch implementation
        is available at \url{https://gitlab.epfl.ch/sinergia/benchmark/src/models/losses}

    \subsubsection{Issue with the naive solution}

        As we do not specify a matching between boxes from $A$ and boxes from
        $B$, but use for each box its closest neighbor (with regards to the
        selected metric) from the opposite list of boxes, it may happen that
        two boxes from $A$ ``match'' to the same box in $B$.

        This can lead to the situation presented in Figure~\ref{fig:non-convexity-naive},
        where our evaluation metric has a local minimum different from the
        global minimum.

        \begin{figure}[ht]
            \centering
            \includegraphics[width=\textwidth, keepaspectratio]{non_convexity_naive}
            \caption[Counter-example on the convexity of the naive solution.]
            {Counter-example on the convexity of the naive solution.
            Here, we move linearly the red box from one item of the ground-truth
            to the other. We display the values of the loss functions for $0.1$
            increments of $\lambda$}
            \label{fig:non-convexity-naive}
        \end{figure}

        Thus, using the naive design of a multi-box loss function could lead
        us to a sub-optimal solution.

    \subsubsection{Solution}

        The naive solution relies heavily on the term
        $\sum[a \in A]{\max[b \in B]{f(a, b)}}$,
        which does not guarantee a one-to-one matching.

        Switching the two operators $\oldsum$ and $\oldmax$ only decreases the
        value of the metric, as the value of $b \in B$ is fixed for all
        $a \in A$. However, we can see that
        $\forall a \in A, \max[b \in B]{f(a, b)} = \oldmax\limits_{\substack{
            \forall b \in B, \lambda_b \in \brc{0, 1} \\
            \sum[b \in B]{\lambda_b} \leq 1}} \lambda_a f(a, b)$.
        Injecting this replacement in the naive solution gives us, for positive
        functions:
        $$\miou(f, A, B) = \frac{1}{\max{\brc{\card{A}, \card{B}}}}
            \oldmax\limits_{\substack{
                \forall a \in A, b \in B, \lambda_{a, b} \in \brc{0, 1} \\
                \forall a \in A, \sum[b \in B]{\lambda_{a, b}} \leq 1 \\
                \forall b \in B, \sum[a \in A]{\lambda_{a, b}} \leq 1}}
            \sum[a \in A]{\sum[b \in B]{
                \lambda_{a, b} f(a, b)}}$$
        As this result is only valid for positive functions, we substract from
        any IoU-based function its minimum value for computing the maximum
        weight matching.

\subsection{Analysis of our final solution}

    In this section, we consider our optimal solution from various angles, verifying that its use is not a burden and can allow using common optimization processes, and that it retains the properties of the underlying functions.

    \subsubsection{Computational cost}

        Linear problems are usually NP-hard. However, in this case, finding
        suitable $\lambda_{a, b}$ is equivalent to solving the problem
        of maximum weight matching in a complete positively weighted bipartite graph.

        Thus we have algorithms with complexity at most $O(V^2 E)$, or using
        the same notations as above: $O(\card{A}^2\card{B}^2)$.

        Table~\ref{table:performance-comparison} presents a comparison of the
        computation times between our naive method and the optimal solution.

        \begin{table}
            \centering
            \resizebox{\textwidth}{!}{%
                \begin{tabular}{|l|l|r|r|r|r|r|}
                    \toprule
                            &   & IoU                 & Generalized IoU     & Distance IoU        & Complete IoU        & Full IoU\\
                    \midrule
                    \multirow{2}{*}{2 boxes} & %
                        Naive   & 2.235 ($\pm$ 0.093) & 2.246 ($\pm$ 0.068) & 2.289 ($\pm$ 0.109) & 2.282 ($\pm$ 0.059) & 2.320 ($\pm$ 0.059) \\
                    \cline{2-7}
                    & Optimal & 2.268 ($\pm$ 0.111) & 2.279 ($\pm$ 0.072) & 2.282 ($\pm$ 0.051) & 2.267 ($\pm$ 0.064) & 2.266 ($\pm$ 0.054) \\
                    \hline
                    \multirow{2}{*}{4 boxes} & %
                        Naive   & 2.207 ($\pm$ 0.031) & 2.221 ($\pm$ 0.038) & 2.201 ($\pm$ 0.056) & 2.204 ($\pm$ 0.031) & 2.214 ($\pm$ 0.027) \\
                    \cline{2-7}
                    & Optimal & 2.279 ($\pm$ 0.043) & 2.277 ($\pm$ 0.072) & 2.274 ($\pm$ 0.128) & 2.254 ($\pm$ 0.041) & 2.218 ($\pm$ 0.052) \\
                    \hline
                    \multirow{2}{*}{16 boxes} & %
                        Naive   & 2.328 ($\pm$ 0.052) & 2.310 ($\pm$ 0.039) & 2.382 ($\pm$ 0.088) & 2.360 ($\pm$ 0.050) & 2.362 ($\pm$ 0.058)\\
                    \cline{2-7}
                    & Optimal & 2.317 ($\pm$ 0.071) & 2.301 ($\pm$ 0.038) & 2.320 ($\pm$ 0.061) & 2.290 ($\pm$ 0.056) & 2.277 ($\pm$ 0.035)\\
                    \hline
                    \multirow{2}{*}{256 boxes} & %
                        Naive   & 10.511 ($\pm$ 0.182) & 10.713 ($\pm$ 0.339) & 11.621 ($\pm$ 1.505) & 10.829 ($\pm$ 0.290) & 11.056 ($\pm$ 0.485)\\
                    \cline{2-7}
                    & Optimal & 10.527 ($\pm$ 0.351) & 10.496 ($\pm$ 0.100) & 10.472 ($\pm$ 0.303) & 10.708 ($\pm$ 0.274) & 10.688 ($\pm$ 0.241)\\
                    \bottomrule
                \end{tabular}
            }
            \caption{Comparison of the computation time (ms) for each method, for lists of 2, 4, 16 and 256 boxes.}\label{table:performance-comparison}
        \end{table}

        We see that the optimal method is not heavier than the naive method in
        terms of needed computation time, and seems even lighter. This
        experiment has been run on a Lenovo T420 running Ubuntu 18.04 LTS, with
        an Intel® Core™ i5-2520M processor, and 16GB of RAM, using the
        implementation present at
        \url{https://gitlab.epfl.ch/sinergia/benchmark/src/models/losses}.

    \subsubsection{Convexity}

        Though our initial goal when crafting our final solution was to achieve
        concavity, this solution is still non-concave, as shown on
        Figure~\ref{fig:non-convexity-optimal}, using the same setting as for
        the previous counter-example.

        \begin{figure}[ht]
            \centering
            \includegraphics[width=\textwidth, keepaspectratio]{non_convexity_optimal}
            \caption[Counter-example on the convexity of the optimal solution.]
            {Counter-example on the convexity of the optimal solution.
            Here, we move linearly the red box from one item of the ground-truth
            to the other. We display the values of the loss functions for $0.1$
            increments of $\lambda$}
            \label{fig:non-convexity-optimal}
        \end{figure}

        Hence, the issue with the convexity of our naive solution has not been
        solved, but we will show in the next section that our optimal is
        suitable for optimization.

        We can also show that no convex function for comparing
        lists of boxes can be crafted without prior knowledge of the matching
        between the boxes of the two lists. Indeed, with $A, B$ two lists of
        boxes, $\sigma$ a permutation on the elements of $B$, and $F$ a
        function comparing two lists of boxes, we would want that
        $F(A, B) = F\prt{A, \sigma(B)}$, thus assuming that $F$ is convex,
        and that $A$ and $\sigma(A)$ are different (a list with two-by-two
        distinct boxes is enough), we have:
        $F(A, A) = F\prt{A, \sigma(A)} = F_{max}$, with $F_{max}$ the maximum
        value of $F$, and
        $\forall \lambda \in \seg[0, 1], F\prt{A, \lambda \sigma(A) + (1 - \lambda) A} = F_{max}$,
        which does not make sense for such a comparison function.

    \subsubsection{Suitability for optimization}

        \paragraph{Maximization}
            Though our final solution is not convex with regards to the
            coordinates of the boxes (as presented on
            Figure~\ref{fig:non-convexity-optimal}), its only local minima are
            the global minima.

            \begin{lemma}
                With $f: \mathcal{A} \times \mathcal{A} \rightarrow \setR_+$
                a positive convave function, such that
                $\forall a, b \in \mathcal{A}, f(a, b) = f(b, a)$ and
                $\forall a \in \mathcal{A}, b \mapsto f(a, b)$ has only
                one maximum, reached in a unique point,
                and $\forall n, m \in \setN,
                F_{n, m}: \mathcal{A}^n \times \mathcal{A}^m \rightarrow \setR_+$,
                $F_{n, m}(A, B) = \oldmax\limits_{\substack{
                    \forall a \in A, b \in B, \lambda_{a, b} \in \brc{0, 1} \\
                    \forall a \in A, \sum[b \in B]{\lambda_{a, b}} \leq 1 \\
                    \forall b \in B, \sum[a \in A]{\lambda_{a, b}} \leq 1}}
                \sum[a \in A]{\sum[b \in B]{\lambda_{a, b} f(a, b)}}$,
                $\forall n, m \in \setN, A \in \mathcal{A}^n$,
                the local maxima of $B \mapsto F_{n, m}(A, B)$
                are exactly its global maxima.
            \end{lemma}

            \begin{proof}
                For $n, m, A$ fixed, let $B^\star$ such that $F_{n, m}(A, B^\star)$
                is a local minimum with regards to $B$. Let us asume that this
                minimum is different from the global minimum.

                As $\forall a, b \in \mathcal{A}, f(a, b) = f(b, a)$,
                $F_{n, m}(A, B) = F_{m, n}(B, A)$, thus, without losing
                generality, we can assume that $n \geq m$. We also know that
                the maximum is reached for any $B \subseteq A$.

                Then, with $A = \cro{a_0, \cdots, a_n}$ and
                $B^\star = \cro{b_0, \cdots, b_m},
                \exists i_0, \cdots, i_m \in \intseg[0, n],
                \lambda_{a_{i_0}, b_0}, \cdots, \lambda_{a_{i_m}, b_m} \in \brc{0, 1}$,
                $\oldmax\limits_{\substack{
                    \forall a \in A, b \in B^\star, \lambda_{a, b} \in \brc{0, 1} \\
                    \forall a \in A, \sum[b \in B^\star]{\lambda_{a, b}} \leq 1 \\
                    \forall b \in B^\star, \sum[a \in A]{\lambda_{a, b}} \leq 1}}
                \sum[a \in A]{\sum[b \in B^\star]{\lambda_{a, b} f(a, b)}} =
                \sum[k = 0][n]{\lambda_{a_{i_k}, b_k} f(a_{i_k}, b_k)}$.
                We assume, without losing generality, that:
                \begin{equation}
                    \forall j \in \intseg[0, m],
                    f(a_{i_j}, b_j) = 0 \Rightarrow \lambda_{a_{i_j}, b_j} = 1
                    \label{eq:lambda_pos}
                \end{equation}

                As we are in a local minimum different from the global minimum,
                either:
                \begin{itemize}
                    \item $\exists j \in \intseg[0, m], \lambda_{a_{i_j}, b_j} = 0$.
                        If $\sum[b \in B^\star]{\lambda_{a_{i_j}, b}} = 0$, then
                        having $\lambda_{a_{i_j}, b_j} = 1$ would not break any
                        constraint, and using~\ref{eq:lambda_pos}, would lead to a
                        higher value: contradiction with the $\oldmax$ building
                        the expression of $F_{n, m}$. Thus
                        $\sum[b \in B^\star]{\lambda_{a_{i_j}, b}} = 0$, but
                        $\sum[a \in A]{\lambda_{a, b_j}} = 0$ and as
                        $\card{A} \geq \card{B^\star}$,
                        $\exists k \in \intseg[0, n], \sum[b \in B^\star]{\lambda_{a_j, b}} = 0$.
                        With $i_j \leftarrow k$ and $\lambda_{a_k, b_j} = 1$,
                        all constraints are still respected, and $f(a_k, b_j) = 0$
                        (otherwise we have a contradiction with the expression of
                        $F_{n, m}$). As $b \mapsto f(a_k, b)$ reaches its maximum
                        in a unique point,
                        $\exists! b_{a_k} \in \mathcal{A}, f(a_k, b_{a_k})$ is a
                        maximum, and as $B^\star$ is a maximum, we have:
                        $b_j = b_{a_k}$, which is in contradiction with
                        $f(a_k, b_j) = 0$. Thus, we have:
                        $\forall j \in \intseg[0, n], \lambda_{a_{i_j}, b_j} = 1$.
                    \item As $\forall j \in \intseg[0, n], \lambda_{a_{i_j}, b_j} = 1$
                        and $B^\star$ is a local maximum and not a global maximum,
                        $\exists j \in \intseg[0, m], f(a_{i_j}, b_j)$ is not a
                        global maximum with regards to $b_j$. However,
                        $b \mapsto f(a_{i_j}, b)$ has only one maximum, thus
                        $\forall \epsilon > 0, \exists a \in \mathcal{A},
                        \nor{a_{i_j} - a} \leq \epsilon \wedge f(a, b) > f(a_{i_j}, b_j)$,
                        and $B^\star$ is not a local minimum.
                \end{itemize}

                We are a contradiction in each case, thus our hypothesis is wrong,
                and $B \mapsto F_{n, m}(A, B)$ reaches a global maximum in
                $B^\star$.
            \end{proof}

            Thus, the gradient-based optimization methods will not stay stuck
            at a local extremum, seeing that such extrema are global extrema,
            thus achieveing our aim.

        \paragraph{Gradient computation}
        As our final function is equal to a sum of the underlying IoU-based
        similarity function, our method keeps the property of the underlying
        function regarding differentiability.

    \subsubsection{Scale-independence}

        As our final function is equal to a sum of the underlying IoU-based
        similarity function, our method keeps the property of the underlying
        function regarding the independence with regards to the scale of the
        image.

        However, the representation of the bounding boxes matters for the
        computation of the functions relying on the penalty term introduced
        for the Complete IoU, if the image is not a square. Indeed, whereas
        for other metrics we just multiply numerator and denominator by the
        width and height of the image, the penalty term of the Complete IoU is
        non-linear, as it includes an $arctan$ term.

        Thus, our solution is scale-independent, but not
        representation-independent, due to the properties of the underlying
        function. Thus, the solution presented above will attribute the same
        importance to all boxes, disregarding their surface, or relative
        importance in the image.

        Note that we can add a normalization surface-related factor in order to
        allow us to prioritize recognizing big boxes with regards to the small
        ones: after having found the $\lambda_{a, b}$ using the expression
        above, we can replace $f(a, b)$ by
        $\frac{\surface{a} + \surface{b}}{\surface{A} + \surface{B}} f(a, b)$.
        This normalization factor will not impact the matching, as it has been
        computed before its introduction, but will weight the boxes with their
        relative importance in the image for the final value of the loss, thus
        will also impact the computed gradients, and prioritize the recognition
        of the boxes with regards to their surface on the image.

    \subsubsection{Boundaries}

        In this section, we show that our method keeps, modulo a factor, the minimum and maximum values of the underlying function.

        \begin{lemma}
            With $f$ with values in $\seg[\alpha, \beta], \forall A \in \mathcal{A}^n, B \in \mathcal{A}^m$, $$\frac{1}{\max{\brc{\card{A}, \card{B}}}}
            \oldmax\limits_{\substack{
                \forall a \in A, b \in B, \lambda_{a, b} \in \brc{0, 1} \\
                \forall a \in A, \sum[b \in B]{\lambda_{a, b}} \leq 1 \\
                \forall b \in B, \sum[a \in A]{\lambda_{a, b}} \leq 1}}
            \sum[a \in A]{\sum[b \in B]{
                \lambda_{a, b} f(a, b)}} \in \seg[
                    \alpha \frac{\min{\brc{\card{A}, \card{B}}}}{\max{\brc{\card{A}, \card{B}}}},
                    \beta \frac{\min{\brc{\card{A}, \card{B}}}}{\max{\brc{\card{A}, \card{B}}}}]$$
        \end{lemma}

        \begin{proof}
            Without losing the generality, we can assume that $\card{A} \geq \card{B}$.

            Let us consider the worst-case scenario:
            $\forall a \in A, b \in B, f(a, b) = \alpha$
            then, with $A = \cro{a_0, \cdots, a_n}, B = \cro{b_0, \cdots, b_m}$,
            $\lambda_{a_i, b_j} = \begin{cases}
                1 \text{ if } i = j \text{ and } i \leq \oldmin \brc{m, n}\\
                0 \text{ otherwise }
            \end{cases}$ is an optimal solution, reaching
            $\alpha \frac{\min{\brc{m, n}}}{\max{\brc{m, n}}}$.

            Similarly, the best-case scenario reaches $\beta \frac{\min{\brc{m, n}}}{\max{\brc{m, n}}}$, thus giving us our two boundaries.
        \end{proof}

        Thus, our method maintains the boundaries of the underlying function, modulo the size of the lists to compare.

        The same reasoning also provides us with the following property of the multi-box version of our functions.

        \begin{property}
            With $A \in \mathcal{A}^n,
                B = \cro{b_0, \cdots, b_m} \in \mathcal{A}^m$,
            and noting $B_k = \cro{b_0, \cdots, b_k}$ and
            for $\sigma \in \mathcal{S}(m)$ a permutation of $\intseg[0, m]$,
            $B_{\sigma, k} = \cro{b_{\sigma(0)}, \cdots, b_{\sigma(k)}}$,
            we have:
            $\forall i \in \intseg[1, n - 1], F_{n, i}(A, B_i) < F_{n, n}(A, B_n)$ and
            $\exists \sigma \in \mathcal{S}(m), \forall i \in \intseg[n + 1, m], F_{n, m}(A, B) \leq F_{n, i}(A, B_{\sigma, i}) \leq F_{n, n}(A, B_{\sigma, n})$
        \end{property}
