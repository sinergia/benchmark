In this section we first evaluate the perfomance of our losses for learning
a set of ``ground-truth'' bounding boxes on their own, then dive into the
impact they have on object detection models, first in one domain, then across
domains, using style-transfer models as backbones.

Our implementation has been written using Pytorch~\cite{NEURIPS2019_9015},
along with Networkx~\cite{hagberg-2008-exploring} for maximum weight matching
algorithm in bipartite graphs, and Scikit-learn~\cite{scikit-learn}.

\subsection{Regression}

    In this section, we present our experimental setting and the results of a
    regression task on our multi-box losses. We also compare the naive to the
    optimal solution.

    \subsubsection{Experimental set up}

        Following the example of Zhang et al.~\cite{zheng_distance-iou_2019}, we
        conduct simulated regression experiments. We adopt a similar procedure
        for testing our multi-box losses:
        \begin{itemize}
            \item we fix the center of our ground-truth boxes
            \item for each ground-truth center, we build a box of area 400 with
            one out of 4 aspect ratios: 1:1, 1:2, 1:3, 1:4
            \item for each ground truth center, we randomly draw 1000 points in
            a 150 radius circle.
            \item these points are the centers of our initial boxes, around which
            we build boxes of varying aspect ratio (same values as above) and
            scale: we multiply the area of the box by 1, 2, 3 or 4.
        \end{itemize}

        Thus, for a target with 2 centers, we build 16 different ground truths.
        For each of the initial centers, we create 16 different initial boxes.
        Thus for a target with 2 centers, we have
        $4^2 \times 1000 \times 4^2 = 256 000$ runs.
        Depending on the machine, one run takes from 6 to 150 seconds.
        Thus, running this experiment for only one example of ground-truth could
        take from 17 to 444 days. We thus decided to sample for each ground truth
        from the possible runs in order to make the computation feasible. In the
        case of a list with two bounding boxes, we take uniformly 1/64 of the
        possible experiments, which will take from 6 hours to 5 days to complete.
        This also allows us try out the following different combinations:
        \begin{itemize}
            \item one target box (positioned at the center of ou pseudo-image
            \item 2 target boxes, far apart (one centered in the bottom right
                corner, the other in the top left corner)
            \item 2 target boxes, close to each other (at distance
                $30\sqrt{2}$ apart): the boxes are not overlapping, but the circles
                in with the initial boxes are drawn do
            \item 4 target boxes, far apart (one in each corner)
            \item 4 target boxes, close to each other (at distance $30$ apart)
        \end{itemize}

        We run the regression on 200 epochs, implementing manually the gradient
        descent. We adjust the learning rate from 0.1 initially to 0.01 after the
        160\textsuperscript{th} epoch, then 0.001 for the last 20 epochs.
        Mimicking the set-up of~\cite{zheng_distance-iou_2019}, we move in the
        opposite direction to the gradients use the update step
        $P_{t + 1} \leftarrow lr \times \prt{H + W} \times \prt{2 - IoU(G, P_t)} \times \nabla f(G, P_t)$,
        with $P_t$ the pseudo-predicted boxes at iteration $t$, $G$ the target
        list of boxes, and $H$ and $W$ the height, respectively width of the
        pseudo-image we consider. We added the $(H + W)$ term in order to account
        for the discrepancy of box representation between their code and ours, and
        speed up the convergence of the regression. The whole code can be found at
        \url{https://gitlab.epfl.ch/sinergia/benchmark/blob/master/iou_simulation.py}.

        The performance of the regression is measured according to the IoU between
        the two lists of boxes, using the optimal solution.

    \subsubsection{Results for the regression on lists of one box}

        Figure~\ref{fig:regression-one} shows that our naive and optimal solutions
        behave the same for lists of one box. It also establishes the baseline
        behavior of the different IoU-based functions in our implementation. We
        see that Distance IoU and Complete IoU can be considered equal, as they
        cannot be distinguished on the chart. We also see that Generalized IoU and
        Complete IoU are fairly similar in terms of convergence time. However,
        Full IoU converges quite faster than both of them.

        \begin{figure}[ht]
            \centering
            \includegraphics[width=0.6\textwidth, keepaspectratio]{optimal_ious_one_box}
            \caption{Error sum of the regression on lists of one box, depending on
            the regression step number.}\label{fig:regression-one}
        \end{figure}

        We also present on Figure~\ref{fig:regression-one-stats} the distribution
        of the final value of the IoU, in other words, the value of the IoU for
        the boxes reached avec the 200 epochs of our regression. We see that the
        Generalized IoU has a slightly higher, thus worse, median and mean, but a
        lower standard deviation. This indicates that the Generalized IoU
        performs in average worse than the other functions, but works better for
        the worst examples.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{violin_ious_one_box}
                \caption{Distribution and median.}
                \label{fig:regression-one-violin}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\linewidth, keepaspectratio]{boxplot_ious_one_box}
                \caption{Mean and standard deviation.}
                \label{fig:regression-one-boxplot}
            \end{subfigure}
            \caption{Statistics of the final IoU values reached during one-box
            regression.}\label{fig:regression-one-stats}
        \end{figure}

    \subsubsection{Results for the regression on lists of two boxes}

        We distinguished two different settings: when the two target boxes are far
        apart, and when the target boxes are close to each other. Indeed, we expect
        our naive solution to counter-perform in the second case, while giving the
        same results as the optimal solution in the first case.

        Figure~\ref{fig:comparison-ious-2distinct} illustrates the second case.
        We plot on the same chart the IoU errors achieved at each step of our
        regression for the Generalized IoU, the Distance IoU, the Complete IoU
        , and the Full IoU, pictured respectively on
        Figures~\ref{fig:comparison-giou-2distinct},
        \ref{fig:comparison-diou-2distinct}, \ref{fig:comparison-ciou-2distinct},
        and~\ref{fig:comparison-fiou-2distinct}. As the learning curves of naive
        and optimal functions are indistinguishable, our hyposthesis is confirmed
        for the case where boxes are far apart.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_generalized_ious_2distinct}
                \caption{Generalized IoU}
                \label{fig:comparison-giou-2distinct}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_distance_ious_2distinct}
                \caption{Distance IoU}
                \label{fig:comparison-diou-2distinct}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_complete_ious_2distinct}
                \caption{Complete IoU}
                \label{fig:comparison-ciou-2distinct}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_full_ious_2distinct}
                \caption{Full IoU}
                \label{fig:comparison-fiou-2distinct}
            \end{subfigure}
            \caption{Comparison of naive and optimal solutions when the target boxes, thus the inital predictions, are far apart.}
            \label{fig:comparison-ious-2distinct}
        \end{figure}

        We plot on Figure~\ref{fig:comparison-ious-2overlapping} the case where
        our target boxes are close from one another. We see for each loss function
        a clear discrepancy between optimal and naive solution, confirming our
        expectation.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_generalized_ious_2overlapping}
                \caption{Generalized IoU}
                \label{fig:comparison-giou-2overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_distance_ious_2overlapping}
                \caption{Distance IoU}
                \label{fig:comparison-diou-2overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_complete_ious_2overlapping}
                \caption{Complete IoU}
                \label{fig:comparison-ciou-2overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_full_ious_2overlapping}
                \caption{Full IoU}
                \label{fig:comparison-fiou-2overlapping}
            \end{subfigure}
            \caption{Comparison of naive and optimal solutions when the target boxes, thus the inital predictions, are close from one another.}
            \label{fig:comparison-ious-2overlapping}
        \end{figure}

        We present on Figure~\ref{fig:regression-2boxes} the evolution of the IoU
        error throughout our regression on lists of two boxes.
        Figure~\ref{fig:regression-2boxes-overlapping} illustrate the learning
        process of our losses where the two target boxes are close from one another,
        while Figure~\ref{fig:regression-2boxes-distinct} illustrate it when theses
        boxes are far from one another. We see that the convergence speed is fairly
        similar in both cases.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{optimal_ious_2overlapping}
                \caption{When the two target boxes are close to one another}
                \label{fig:regression-2boxes-overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{optimal_ious_2distinct}
                \caption{When the two target boxes are far apart}
                \label{fig:regression-2boxes-distinct}
            \end{subfigure}
            \caption{Behavior of the different IoU-based losses in the regression
            between lists of two boxes.}\label{fig:regression-2boxes}
        \end{figure}

        \TODO{Update analysis}
        However, we have quite a surprise here: the Generalized IoU seems to learn
        faster than the Distance and Complete IoU when the boxes are far apart.
        Taking a look at the statistics of the final error for the same experiments,
        displayed on Figure~\ref{fig:regression-2boxes-stats}, we see than the
        Generalized IoU has a better median than even the Full IoU, which is still
        performing better than the Complete Iou and the Distance IoU, when the
        target boxes are close to one another (see
        Figures~\ref{fig:regression-2distinct-violin}
        and~\ref{fig:regression-2distinct-boxplot}). However, its first and third
        quartiles are slightly worse than for the Full IoU, and the first quartile
        is worse than any other method in the same case. When target boxes are close
        from one another, see Figures~\ref{fig:regression-2overlapping-violin}
        and~\ref{fig:regression-2overlapping-boxplot}, Generalized IoU does not
        outperform Full IoU, but does for Distance and Complete IoU, in terms of
        median and third quartile. We also see a clear discrepancy between the
        first quartiles of our losses when using the naive solution: Full IoU beats
        the Generalized IoU by more than $0.2$, and both other losses by roughly
        $0.4$.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{violin_ious_2distinct}
                \caption{Distribution and median when the target boxes are far apart.}
                \label{fig:regression-2distinct-violin}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\linewidth, keepaspectratio]{boxplot_ious_2distinct}
                \caption{Mean and standard deviation when the target boxes are far apart.}
                \label{fig:regression-2distinct-boxplot}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{violin_ious_2overlapping}
                \caption{Distribution and median when the target boxes are close
                to each other.}
                \label{fig:regression-2overlapping-violin}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\linewidth, keepaspectratio]{boxplot_ious_2overlapping}
                \caption{Mean and standard deviation when the target boxes are
                close to each other.}
                \label{fig:regression-2overlapping-boxplot}
            \end{subfigure}
            \caption{Statistics of the final IoU values reached during two-box
            regression.}\label{fig:regression-2boxes-stats}
        \end{figure}

    \subsubsection{Results for the regression on lists of four boxes}

        For the regression on lists of four boxes, we distinguish the same cases as
        for lists of two boxes, and compare again on
        Figure~\ref{fig:comparison-ious-4overlapping} the optimal and naive
        solution in the case where
        our target boxes are close from one another. We see for each loss function
        a clear discrepancy between optimal and naive solution, as for the
        two-boxes regression.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_generalized_ious_4overlapping}
                \caption{Generalized IoU}
                \label{fig:comparison-giou-4overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_distance_ious_4overlapping}
                \caption{Distance IoU}
                \label{fig:comparison-diou-4overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_complete_ious_4overlapping}
                \caption{Complete IoU}
                \label{fig:comparison-ciou-4overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.24\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{compare_full_ious_4overlapping}
                \caption{Full IoU}
                \label{fig:comparison-fiou-4overlapping}
            \end{subfigure}
            \caption{Comparison of naive and optimal solutions when the target boxes, thus the inital predictions, are close from one another.}
            \label{fig:comparison-ious-4overlapping}
        \end{figure}

        We present on Figure~\ref{fig:regression-4boxes} the evolution of
        the IoU error throughout our regression on lists of two boxes.
        Figure~\ref{fig:regression-4boxes-overlapping} illustrate the learning
        process of our losses where the two target boxes are close from one another,
        while Figure~\ref{fig:regression-4boxes-distinct} illustrate it when theses
        boxes are far from one another. We see that the convergence speed is fairly
        similar in both cases.

        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{optimal_ious_4overlapping}
                \caption{When the two target boxes are close to one another}
                \label{fig:regression-4boxes-overlapping}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{optimal_ious_4distinct}
                \caption{When the two target boxes are far apart}
                \label{fig:regression-4boxes-distinct}
            \end{subfigure}
            \caption{Behavior of the different IoU-based losses in the regression
            between lists of two boxes.}\label{fig:regression-4boxes}
        \end{figure}


        \begin{figure}[ht]
            \centering
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{violin_ious_4distinct}
                \caption{Distribution and median when the target boxes are far apart.}
                \label{fig:regression-4distinct-violin}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\linewidth, keepaspectratio]{boxplot_ious_4distinct}
                \caption{Mean and standard deviation when the target boxes are far apart.}
                \label{fig:regression-4distinct-boxplot}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\textwidth, keepaspectratio]{violin_ious_4overlapping}
                \caption{Distribution and median when the target boxes are close
                to each other.}
                \label{fig:regression-4overlapping-violin}
            \end{subfigure}
            \hfill
            \begin{subfigure}{0.49\textwidth}
                \includegraphics[width=\linewidth, keepaspectratio]{boxplot_ious_4overlapping}
                \caption{Mean and standard deviation when the target boxes are
                close to each other.}
                \label{fig:regression-4overlapping-boxplot}
            \end{subfigure}
            \caption{Statistics of the final IoU values reached during two-box
            regression.}\label{fig:regression-4boxes-stats}
        \end{figure}

        \TODO{Complete analysis, with the analysis for 4 boxes once the results
        are present}

\subsection{Object detection}

    In this section, we analyze the impact of our multi-box losses on
    Faster-RCNN~\cite{ren_faster_2016}, an object detection model outputting
    only the final bounding boxes. As the need for our solution could be
    alleviated in anchors-based models, like YOLO~\cite{redmon_you_2016} or
    RetinaNet~\cite{lin_focal_2018}, we decided to not use these models for
    evaluating the perfomance of our metrics.

    We use the implementation of Faster RCNN provided by the torchvision
    package. We ran this model on CityPersons (using 1893 images) and
    Pascal VOC (using 3000 images), with and without the multi-box loss
    functions.
    \g{Should I compute the mAP on a test set ?}

    Figure~\ref{fig:example-box-pascal} presents examples of Pascal VOC images
    with the bounding boxes predicted by the different models.

    \begin{figure}[ht]
        \centering
        \TODO{Add image}
        \caption{Bounding box prediction on Pascal VOC.}
        \label{fig:example-box-pascal}
    \end{figure}

    \g{Should I present the evolution of the loss through training epochs ?}

    \subsection{Impact on style-transfer}

    In this section, we introduce our design of an object-detection resilient
    style-transfer model, and its eperimental performance.

    \subsubsection{Model}

        Given a style-transfer model, e.g. CycleGan~\cite{zhu_unpaired_2017},
        UNIT~\cite{liu_unsupervised_2017}, MUNIT~\cite{ferrari_multimodal_2018},
        DRIT~\cite{lee_diverse_2018}, we train an object detection model, e.g.
        Faster RCNN on one domain, then train the style-transfer model across
        domains while adding a loss: we compare the result of the pretrained object
        detection on the style-transfered image to the ground-truth of the original
        image. This structure is presented on
        Figure~\ref{fig:object-detection-resilient}.

        \begin{figure}[!htp]
            \centering
            \begin{tikzpicture}
                % domain A
                \node [draw, align=center] (A) {$IMG_A$};
                % domain B
                \node [draw, align=center, right=10em of A] (B) {$IMG_{A'}$};
                % ground-truth bounding boxes for A
                \node [draw, align=center, below=1ex of A] (GTA) {$GT_A$};
                % predicted bounding boxes for B
                \node [draw, align=center, below=2em of B] (PB) {$P_{A'}$};
                % link style transfer
                \path[draw, bend left, ->, out=20, in=160, looseness=.5] (A) to node[above] {Style transfer} (B);
                % link prediction
                \path[draw, bend left, ->, out=20, in=160, looseness=.5] (B) to node[right] {Bounding box prediction} (PB);
                % link loss
                \path[draw, dotted, bend left, <->, out=-20, in=-160, looseness=.5] (GTA) to node[below] {Bounding boxes loss} (PB);
            \end{tikzpicture}
            \caption{Presentation of our object detection-resilient style-transfer
            model.}\label{fig:object-detection-resilient}
        \end{figure}

        Note that the potential style vector or image is not represented, as it
        does not matter in our model. Also, for multi-modal style-transfer models,
        we still train the object detection model on one domain only, and then use
        the multi-box IoU-based loss for each other domain.

    \subsubsection{Experimental setup}

        We trained our style-transfer models on the task of transfering between
        Pascal VOC~\cite{pascal-voc-2012} and Comics2k~\cite{inoue_2018_cvpr} for
        bimodal models (UNIT~\cite{liu_unsupervised_2017}), and between
        Pascal VOC~\cite{pascal-voc-2012}, Comics2k~\cite{inoue_2018_cvpr},
        Clipart1k~\cite{inoue_2018_cvpr}, and
        Watercolor2k~\cite{inoue_2018_cvpr} for
        multi-modal models (MUNIT~\cite{ferrari_multimodal_2018} and
        DRIT~\cite{lee_diverse_2018}). We limit the datasets to 2000 images
        due to time constraints.

        These models are implemented in their own folder in
        \url{https://gitlab.epfl.ch/sinergia/benchmark/tree/master/src/models}.
        our architecture build around the easiness of extensibility, adding
        our IoU losses is done through a mixin, defined at
        \url{https://gitlab.epfl.ch/sinergia/benchmark/blob/master/src/models/object_detection/mixin.py}.

    \subsubsection{Experiments results}

